package com.fma.service;

import static com.fma.util.EmailUtil.FROM_ADDRESS;
import static com.fma.util.EmailUtil.FROM_PASSWORD;
import static com.fma.util.EmailUtil.MAIL_BODY;
import static com.fma.util.EmailUtil.MAIL_SUBJECT;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.fma.dao.UserDao;
import com.fma.model.User;
import com.fma.util.EmailUtil;

@Component
public class UserService {

	private UserDao userDao = new UserDao();

	public List<User> getUsers() {
		System.out.println("fetching all Users");
		return userDao.getUsers();
	}

	public Optional<User> getUser(String email) {
		System.out.println("fetching User : " + email);
		return userDao.getUser(email);
	}

	public boolean deleteUser(String email) {
		System.out.println("deleting User : " + email);
		return userDao.deleteUser(email);
	}

	public Boolean verifyUser(User userToVerify) {
		System.out.println("verifying user User : email : " + userToVerify.getEmail() + ", vcode : "
				+ userToVerify.getVerificationCode());
		return userDao.verify(userToVerify).isPresent();
	}

	public boolean updateUser(User user) {
		System.out.println("updating User");
		return userDao.updateUser(user);
	}
	
	public String generateVerficationCode() {
		return Integer.toString(new Random().nextInt(10000));
	}

	public boolean sendVerificationCode(User user) {
		System.out.println("Generated Verification Code : " + user.getVerificationCode());
		System.out.println("Sending email to : " + user.getEmail());
		try {
			EmailUtil.send(FROM_ADDRESS, FROM_PASSWORD, user.getEmail(), MAIL_SUBJECT, MAIL_BODY.concat("1543"));
		} catch (Exception e) {
			System.out.println("Exception occurred while sending verification code..!!");
			e.printStackTrace();
			return false;
		}
		System.out.println("Email Sent Successfully..!!");
		return true;
	}
}
