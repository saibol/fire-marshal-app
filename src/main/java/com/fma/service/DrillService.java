package com.fma.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.fma.dao.DrillDao;
import com.fma.model.Drill;
import com.fma.model.DrillUser;

@Component
public class DrillService {

	private DrillDao drillDao = new DrillDao();

	public Optional<Drill> getDrill() {
		System.out.println("fetching drill");
		return drillDao.getDrill();
	}

	public List<DrillUser> getDrillDetails() {
		System.out.println("fetching getDrillDetails");
		return drillDao.getDrillDetails();
	}
	
	public Optional<DrillUser> updateDrillUser(String email) {
		System.out.println("fetching getDrillDetail for email : "+email);
		return drillDao.updateDrillUser(email);
	}
}
