package com.fma.model;

import java.time.LocalDateTime;

public class Drill {
	private Integer drillId;
	private String drillType;
	private LocalDateTime drillStartTime;
	private LocalDateTime drillEndTime;
	private String note;
	private Integer initiatedBy;
	private Boolean isActive;
	
	public Drill(){
		drillStartTime = LocalDateTime.now();
		isActive = true;
	}
	
	public Drill(Integer drillId, String drillType, LocalDateTime drillStartTime, LocalDateTime drillEndTime,
			String note, Integer initiatedBy, Boolean isActive) {
		super();
		this.drillId = drillId;
		this.drillType = drillType;
		this.drillStartTime = drillStartTime;
		this.drillEndTime = drillEndTime;
		this.note = note;
		this.initiatedBy = initiatedBy;
		this.isActive = isActive;
	}
	public Integer getDrillId() {
		return drillId;
	}
	public void setDrillId(Integer drillId) {
		this.drillId = drillId;
	}
	public String getDrillType() {
		return drillType;
	}
	public void setDrillType(String drillType) {
		this.drillType = drillType;
	}
	public LocalDateTime getDrillStartTime() {
		return drillStartTime;
	}
	public void setDrillStartTime(LocalDateTime drillStartTime) {
		this.drillStartTime = drillStartTime;
	}
	public LocalDateTime getDrillEndTime() {
		return drillEndTime;
	}
	public void setDrillEndTime(LocalDateTime drillEndTime) {
		this.drillEndTime = drillEndTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Integer getInitiatedBy() {
		return initiatedBy;
	}
	public void setInitiatedBy(Integer initiatedBy) {
		this.initiatedBy = initiatedBy;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "Drill [drillId=" + drillId + ", drillType=" + drillType + ", drillStartTime=" + drillStartTime
				+ ", drillEndTime=" + drillEndTime + ", note=" + note + ", initiatedBy=" + initiatedBy + ", isActive="
				+ isActive + "]";
	}
}
