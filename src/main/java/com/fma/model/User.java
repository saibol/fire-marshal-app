package com.fma.model;

public class User {
	private Integer userId;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String city;
	private String role;
	private Integer locationFloor;
	private String locationDesk;
	private Integer reportingTo;
	private Boolean isRegistered;
	private String verificationCode;
	private Boolean isActive;
	
	public User(){
		city = "Pune";
		isRegistered = false;
		isActive = true;
	}
	
	public User(Integer userId, String firstName, String lastName, String email, String mobile, String city,
			String role, Integer locationFloor, String locationDesk, Integer reportingTo, Boolean isRegistered,
			String verificationCode, Boolean isActive) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.mobile = mobile;
		this.city = city;
		this.role = role;
		this.locationFloor = locationFloor;
		this.locationDesk = locationDesk;
		this.reportingTo = reportingTo;
		this.isRegistered = isRegistered;
		this.verificationCode = verificationCode;
		this.isActive = isActive;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getLocationFloor() {
		return locationFloor;
	}
	public void setLocationFloor(Integer locationFloor) {
		this.locationFloor = locationFloor;
	}
	public String getLocationDesk() {
		return locationDesk;
	}
	public void setLocationDesk(String locationDesk) {
		this.locationDesk = locationDesk;
	}
	public Integer getReportingTo() {
		return reportingTo;
	}
	public void setReportingTo(Integer reportingTo) {
		this.reportingTo = reportingTo;
	}
	public Boolean getIsRegistered() {
		return isRegistered;
	}
	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", mobile=" + mobile + ", city=" + city + ", role=" + role + ", locationFloor=" + locationFloor
				+ ", locationDesk=" + locationDesk + ", reportingTo=" + reportingTo + ", isRegistered=" + isRegistered
				+ ", verificationCode=" + verificationCode + ", isActive=" + isActive + "]";
	}
}

