package com.fma.model;

import java.time.LocalTime;

public class DrillUser {
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private Integer locationFloor;
	private String locationDesk;
	private Boolean isSafe;
	private LocalTime reportTime;
	
	public DrillUser(String firstName, String lastName, String email, String mobile, Integer locationFloor,
			String locationDesk) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.mobile = mobile;
		this.locationFloor = locationFloor;
		this.locationDesk = locationDesk;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getLocationFloor() {
		return locationFloor;
	}
	public void setLocationFloor(Integer locationFloor) {
		this.locationFloor = locationFloor;
	}
	public String getLocationDesk() {
		return locationDesk;
	}
	public void setLocationDesk(String locationDesk) {
		this.locationDesk = locationDesk;
	}
	public Boolean getIsSafe() {
		return isSafe;
	}
	public void setIsSafe(Boolean isSafe) {
		this.isSafe = isSafe;
	}
	public LocalTime getReportTime() {
		return reportTime;
	}
	public void setReportTime(LocalTime reportTime) {
		this.reportTime = reportTime;
	}
}
