package com.fma.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.fma.model.User;

public class UserDao {
	
	public static List<User> users;
	
	public UserDao(){
		User m1 = new User(1,"shraddha","joshi","shraddhajoshi@gmail.com","9822794700","Pune","Manager",6,"110",null,true,"1210",true);
		User m2 = new User(2,"Devesh","Gote","deveshgote@gmail.com","9822794701","Pune","Manager",6,"111",null,true,"1211",true);
		User m3 = new User(3,"Pradipta","Mohanty","pradiptamohanty@gmail.com","9822794702","Pune","Manager",3,"112",null,true,"1212",true);

		User e1 = new User(4,"sunil","saibol","sunilsaibol@gmail.com","9822794703","Pune","Employee",6,"113",1,true,"1213",true);
		User e2 = new User(5,"ganesh","khutwad","ganeshkhutwad30690@gmail.com","9822794704","Pune","Employee",6,"114",1,true,"1214",true);
		User e3 = new User(6,"shweta","kanungo","shwetakanungo@gmail.com","9822794705","Pune","Employee",6,"115",1,true,"1215",true);
		User e4 = new User(7,"payal","kankariya","payalkankariya@gmail.com","9822794706","Pune","Employee",4,"116",2,true,"1216",true);
		User e5 = new User(8,"sanjeevani","jadhav","sanjeevanijadhav@gmail.com","9822794707","Pune","Employee",4,"117",2,true,"1217",false);
		User e6 = new User(9,"priya","jaroli","priyajaroli@gmail.com","9822794708","Pune","Employee",4,"118",2,true,"1218",true);
		User e7 = new User(10,"ram","zunzerke","zunzerke@gmail.com","9822794709","Pune","Employee",3,"119",3,true,"1219",true);
		User e8 = new User(11,"roshan","kuchankar","kuchankar@gmail.com","9822794710","Pune","Employee",3,"120",3,true,"1220",true);
		User e9 = new User(12,"jitendra","tayade","jitendratayade@gmail.com","9822794711","Pune","Employee",6,"121",1,true,"1221",true);
		User e10 = new User(13,"sachin","bhat","sachinbhat@gmail.com","9822794712","Pune","FireMarshal",6,"122",1,true,"1222",true);
		
		users = new ArrayList<User>(Arrays.asList(m1,m2,m3,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10));
	}
	
	public List<User> getUsers() {
		return users;
	}
	
	public Optional<User> getUser(String email){
		return getUsers().stream().filter(user -> !Objects.isNull(user.getEmail()) && user.getEmail().equals(email) && user.getIsActive())
				.findFirst();
	}
	
	public Optional<User> verify(User userToVerify) {
		return getUsers().stream()
				.filter(user -> !Objects.isNull(userToVerify.getEmail())
						&& !Objects.isNull(userToVerify.getVerificationCode())
						&& userToVerify.getEmail().equals(userToVerify.getEmail())
						&& userToVerify.getVerificationCode().equals(userToVerify.getVerificationCode())
						&& userToVerify.getIsActive())
				.findFirst();
	}

	public boolean deleteUser(String email){
		return users.removeIf(user -> !Objects.isNull(user.getEmail()) && user.getEmail().equals(email));
	}
	
	public boolean updateUser(User userToUpdate){
		Optional<User> user = getUser(userToUpdate.getEmail());
		if(user.isPresent()) {
			User u = user.get();
			u.setFirstName(userToUpdate.getFirstName());
			u.setLastName(userToUpdate.getLastName());
//			u.setEmail(userToUpdate.getEmail()); // not allowed to update email
			u.setMobile(userToUpdate.getMobile());
			u.setCity(userToUpdate.getCity());
			u.setRole(userToUpdate.getRole());
			u.setLocationFloor(userToUpdate.getLocationFloor());
			u.setLocationDesk(userToUpdate.getLocationDesk());
			u.setReportingTo(userToUpdate.getReportingTo());
			u.setIsRegistered(userToUpdate.getIsRegistered());
			u.setVerificationCode(userToUpdate.getVerificationCode());
			u.setIsActive(userToUpdate.getIsActive());
			u.setFirstName(userToUpdate.getFirstName());
		}
		return user.isPresent();
	}
}
