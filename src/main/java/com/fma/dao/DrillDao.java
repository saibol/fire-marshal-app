package com.fma.dao;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.fma.model.Drill;
import com.fma.model.DrillUser;

public class DrillDao {

	public static List<Drill> drills;
	public static List<DrillUser> drillDetails = new ArrayList<>();

	public DrillDao() {
		Drill d1 = new Drill(1, "Mock", LocalDateTime.now(), LocalDateTime.now(), "notes", 2, true);
		Drill d2 = new Drill(2, "Mock", LocalDateTime.now(), LocalDateTime.now(), "notes", 2, false);
		drills = new ArrayList<Drill>(Arrays.asList(d1, d2));

		new UserDao().getUsers().stream().filter(a -> a.getIsActive()).forEach(u -> {
			DrillUser drillUser = new DrillUser(u.getFirstName(), u.getLastName(), u.getEmail(), u.getMobile(),
					u.getLocationFloor(), u.getLocationDesk());
			drillUser.setIsSafe(false);
			drillDetails.add(drillUser);
		});
	}

	public List<Drill> getDrills() {
		return drills;
	}

	public Optional<Drill> getDrill() {
		return getDrills().stream().filter(drill -> drill.getIsActive()).findFirst();
	}

	public List<DrillUser> getDrillDetails() {
		return drillDetails;
	}

	public Optional<DrillUser> getDrillUser(String email) {
		return drillDetails.stream().filter(du -> !Objects.isNull(email) && du.getEmail().equals(email)).findFirst();
	}
	
	public Optional<DrillUser> updateDrillUser(String email) {
		Optional<DrillUser> drillUserOp = getDrillUser(email);
		if(drillUserOp.isPresent()) {
			DrillUser drillUser = drillUserOp.get();
			drillUser.setIsSafe(!drillUser.getIsSafe());
			drillUser.setReportTime(LocalTime.now());
		}
		return drillUserOp;
	}
}
