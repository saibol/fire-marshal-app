package com.fma.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fma.model.User;
import com.fma.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/")
	@ResponseBody
	public List<User> getAllUsers() {
		return userService.getUsers();
	}

	@GetMapping("/{email}")
	@ResponseBody
	public ResponseEntity<User> getUser(@PathVariable("email") String email) {
		Optional<User> user = userService.getUser(email);
		return user.isPresent() ? new ResponseEntity<User>(user.get(), HttpStatus.OK)
				: new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{email}/notify")
	@ResponseBody
	public ResponseEntity<Boolean> notify(@PathVariable("email") String email) {
		User user = new User();
		user.setEmail(email);
		user.setVerificationCode(userService.generateVerficationCode());
		
		Optional<User> userToUpdate = userService.getUser(email);
		userToUpdate.get().setVerificationCode(user.getVerificationCode());
		userService.updateUser(userToUpdate.get());
		System.out.println("user DB updated with verification code");
		
		return userService.sendVerificationCode(user) 
				? new ResponseEntity<>(HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
	}
	
	@DeleteMapping("/{email}")
	@ResponseBody
	public ResponseEntity<Boolean> deleteUsers(@PathVariable("email") String email) {
		return userService.deleteUser(email)
				? new ResponseEntity<Boolean>(HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping("/")
	@ResponseBody
	public ResponseEntity<Boolean> updateUsers(@RequestBody User userToUpdate) {
		return userService.updateUser(userToUpdate)
				? new ResponseEntity<>(HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{email}/verify/{vcode}")
	@ResponseBody
	public ResponseEntity<Boolean> verify(@PathVariable("email") String email, @PathVariable("vcode") String vcode) {
		User user = new User();
		user.setEmail(email);
		user.setVerificationCode(vcode);
		return userService.verifyUser(user)
				? new ResponseEntity<>(HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}	
}
