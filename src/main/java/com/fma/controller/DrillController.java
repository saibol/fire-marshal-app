package com.fma.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fma.model.Drill;
import com.fma.model.DrillUser;
import com.fma.service.DrillService;

@RestController
@RequestMapping("/drills")
public class DrillController {

	@Autowired
	private DrillService drillService;

	@GetMapping("/users")
	@ResponseBody
	public ResponseEntity<List<DrillUser>> getDrillDetails() {
		List<DrillUser> drillDetail = drillService.getDrillDetails();
		return drillDetail != null ? new ResponseEntity<List<DrillUser>>(drillDetail, HttpStatus.OK)
				: new ResponseEntity<List<DrillUser>>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/")
	@ResponseBody
	public ResponseEntity<Drill> getDrill() {
		Optional<Drill> drill = drillService.getDrill();
		return drill.isPresent() ? new ResponseEntity<Drill>(drill.get(), HttpStatus.OK)
				: new ResponseEntity<Drill>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/users/{email}")
	@ResponseBody
	public ResponseEntity<DrillUser> getDrillDetail(@PathVariable("email") String email) {
		Optional<DrillUser> drillDetail = drillService.updateDrillUser(email);
		return drillDetail.isPresent() ? new ResponseEntity<DrillUser>(drillDetail.get(), HttpStatus.OK)
				: new ResponseEntity<DrillUser>(HttpStatus.NO_CONTENT);
	}
}
